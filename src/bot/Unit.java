package bot;

public enum Unit {
    player1('a'), player2('b'), empty('.'), neutralWall('#'), wall1('1'), wall2('2'), gold('*');
    private final char ch;

    private Unit(char ch) {
        this.ch = ch;
    }

    public char getChar() {
        return ch;
    }
}
