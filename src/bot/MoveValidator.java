package bot;

import java.util.*;

import static bot.Unit.*;
import static bot.GameState.N;
import static bot.Utils.*;


public class MoveValidator {

    private MoveValidator() {}

    public static boolean isValidMove(GameState gameState, List<Position> moves, int steps) {
        Unit whoMoves = gameState.whatUnitMovesNow();
        Unit[][] board = gameState.getBoard();
        Position curPos = positionOf(board, whoMoves);
        int sumLen = 0;
        for (Position to : moves) {
            if (to.equals(curPos)) {
                return false;
            }
            if (!inside(to.x, to.y)) {
                return false;
            }
            Unit value = board[to.x][to.y];
            if (value != empty && value != gold && value != whoMoves) {
                return false;
            }
            int[][] dist = gameState.bfs(whoMoves, curPos); // don't modify!
            sumLen += dist[to.x][to.y];
            curPos = to;
        }

        return sumLen <= steps;
    }

    public static boolean mayPlaceWall(GameState gameState, Position to) {
        Unit[][] board = gameState.getBoard();
        if (board[to.x][to.y] != empty) {
            return false;
        }
        Unit whoNotMoves = gameState.whatUnitMovesNow() == player1 ? player2 : player1;
        boolean[][] cutPoint = gameState.cutPoint;
        if(cutPoint == null) {
            cutPoint = CutPointsFinder.findCutPoints(gameState, whoNotMoves);
            gameState.cutPoint = cutPoint;
        }
        return !cutPoint[to.x][to.y];
    }

    static Position positionOf(Unit[][] board, Unit unit) {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (board[i][j] == unit) {
                    return new Position(i, j);
                }
            }
        }
        System.out.print(unit + " not found");
        throw new RuntimeException();
    }
}
