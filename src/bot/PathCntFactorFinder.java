package bot;

import java.util.Arrays;

import static bot.GameState.N;
import static java.lang.Math.min;

public class PathCntFactorFinder {
    private static final int VISITED0 = 0;
    private static final int VISITED1 = 1;
    private static final int VISITED2 = 2;

    public static int findNumberOfCellsOnSomeSimplePath(GameState gameState, Unit player, Position a, Position b) {
        int[][] answer = findCellsOnSomeSimplePath(gameState, player, a, b);
        int r = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if(answer[i][j] == 1) {
                    r++;
                }
            }
        }
        return r;
    }

    public static int[][] findCellsOnSomeSimplePath(GameState gameState, Unit player, Position a, Position b) {
        int[][] dist = Utils.getStrictWallsForPlayer(gameState.getBoard(), player);
        findCutPoints(dist, a);
        //print(dist, cutPoint);

        Arrays.fill(goodColor, false);
        Utils.fill(answer, 0);
        dfs1(dist, a.x, a.y, maxColor - 1, -1, -1);

        //print(dist, colorVert);
        //print(dist, colorHor);

        dfs3(dist, a.x, a.y, b.x, b.y);

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (dist[i][j] != Utils.WALL && isGood(i, j)) {
                    answer[i][j] = 1;
                }
            }
        }
        //print(dist, answer);

        /*for (int i = 0; i < maxColor; i++) {
            System.out.println(goodColor[i]);
        }/**/

        return answer;
    }

    private static boolean isGood(int x, int y) {
        if (goodColor[colorVert[x][y]] || goodColor[colorHor[x][y]]) {
            return true;
        }
        if (x > 0) {
            if (goodColor[colorVert[x - 1][y]]) {
                return true;
            }
        }
        if (y > 0) {
            if (goodColor[colorHor[x][y - 1]]) {
                return true;
            }
        }
        return false;
    }

    private static boolean dfs3(int[][] dist, int x, int y, int fx, int fy) {
        dist[x][y] = VISITED2;
        if (x == fx && y == fy) {
            return true;
        }
        for (int d = 0; d < 4; d++) {
            int tox = x + Utils.dx[d];
            int toy = y + Utils.dy[d];
            if (!Utils.inside(tox, toy)) {
                continue;
            }
            if (dist[tox][toy] != VISITED1) {
                continue;
            }
            if (dfs3(dist, tox, toy, fx, fy)) {
                markGood(x, y, tox, toy);
                return true;
            }
        }
        return false;
    }

    private static void markGood(int x, int y, int tox, int toy) {
        if (x == tox) {
            goodColor[colorHor[min(x, tox)][min(y, toy)]] = true;
        } else {
            goodColor[colorVert[min(x, tox)][min(y, toy)]] = true;
        }
    }

    private static void print(int[][] dist, int[][] t) {
        System.out.println();
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                System.out.print(t[i][j]);
                /*if (dist[i][j] == Utils.WALL) {
                    System.out.print('#');
                } else {
                    System.out.print(t[i][j]);
                }
                System.out.print('\t');/**/
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void findCutPoints(int[][] dist, Position start) {
        timer = 0;
        //Utils.fill(tin, 0);
        //Utils.fill(fup, 0);
        Utils.fill(cutPoint, 0);
        Utils.fill(colorHor, 0);
        Utils.fill(colorVert, 0);
        maxColor = 1;
        dfs0(dist, start.x, start.y, -1, -1);
    }

    private static int maxColor = 1;
    private static int timer;
    private static int[][] tin = new int[N][N];
    private static int[][] fup = new int[N][N];
    private static int[][] cutPoint = new int[N][N];
    private static boolean[] goodColor = new boolean[N * N + N];
    private static int[][] colorHor = new int[N][N];
    private static int[][] colorVert = new int[N][N];
    private static int[][] answer = new int[N][N];

    private static void dfs0(int[][] dist, int x, int y, int parentX, int parentY) {
        dist[x][y] = VISITED0;
        tin[x][y] = fup[x][y] = timer++;
        int children = 0;
        for (int d = 0; d < 4; d++) {
            int tox = x + Utils.dx[d];
            int toy = y + Utils.dy[d];
            if (!Utils.inside(tox, toy)) {
                continue;
            }
            if (tox == parentX && toy == parentY || dist[tox][toy] == Utils.WALL) {
                continue;
            }
            if (dist[tox][toy] != Utils.NOT_VISITED)
                fup[x][y] = min(fup[x][y], tin[tox][toy]);
            else {
                dfs0(dist, tox, toy, x, y);
                fup[x][y] = min(fup[x][y], fup[tox][toy]);
                if (fup[tox][toy] >= tin[x][y] && parentX != -1)
                    cutPoint[x][y] = 1;
                children++;
            }
        }
        if (parentX == -1 && children >= 2) {
            cutPoint[x][y] = 1;
        }
    }

    static void dfs1(int[][] dist, int x, int y, int col, int parentX, int parentY) {
        dist[x][y] = VISITED1;
        for (int d = 0; d < 4; d++) {
            int tox = x + Utils.dx[d];
            int toy = y + Utils.dy[d];
            if (!Utils.inside(tox, toy)) {
                continue;
            }
            if (tox == parentX && toy == parentY || dist[tox][toy] == Utils.WALL) {
                continue;
            }
            if (dist[tox][toy] == VISITED0) {
                if (fup[tox][toy] >= tin[x][y]) {
                    maxColor++;
                    paint(x, y, tox, toy, maxColor - 1);
                    dfs1(dist, tox, toy, maxColor - 1, x, y);
                } else {
                    paint(x, y, tox, toy, col);
                    dfs1(dist, tox, toy, col, x, y);
                }
            } else {
                if (tin[tox][toy] <= tin[x][y]) {
                    paint(x, y, tox, toy, col);
                }
            }
        }
    }

    private static void paint(int x, int y, int tox, int toy, int col) {
        if (x == tox) {
            colorHor[min(x, tox)][min(y, toy)] = col;
        } else {
            colorVert[min(x, tox)][min(y, toy)] = col;
        }
    }
}
