package bot;

import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.testng.Assert.*;

@Test
public class BestMoveFinderTest {
    @Test
    void trivial() {
        GameState gameState = new GameState(
                "#####b..\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "#######*\n" +
                        "a......."
                ,
                0
        );

        Object move = new BestMoveFinder(gameState).findBestMove(gameState, 1).move;
        assertEquals(move, Collections.singletonList(new Position(7, 3)));
    }


    @Test
    void minimizePlaceWallCntCounterExample() {
        GameState gameState = new GameState(
                "#####b..\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "2######*\n" +
                        "a......."
                ,
                0
        );

        Object move = new BestMoveFinder(gameState).findBestMove(gameState, 1).move;
        assertEquals(move, Collections.singletonList(new Position(7, 3)));
    }

    @Test
    void testPlaceWall() {
        GameState gameState = new GameState(
                "b.......\n" +
                        "........\n" +
                        "*.......\n" +
                        "a.......\n" +
                        "........\n" +
                        "........\n" +
                        "........\n" +
                        "........"
                ,
                4
        );

        Object move = new BestMoveFinder(gameState).findBestMove(gameState, 2).move;
        assertEquals(move, new Position(1, 0));
    }

    @Test
    void testPlaceWall2() {
        GameState gameState = new GameState(
                "b.......\n" +
                        "........\n" +
                        "*.......\n" +
                        "a.......\n" +
                        "........\n" +
                        "........\n" +
                        "........\n" +
                        "........"
                ,
                4
        );

        Object move = new BestMoveFinder(gameState).findBestMove(gameState, 1).move;
        assertEquals(move, new Position(1, 0));
    }

    @Test
    void testPlaceWall3() {
        GameState gameState = new GameState(
                "b.....1.\n" +
                        "........\n" +
                        "*.......\n" +
                        "a.......\n" +
                        "........\n" +
                        "........\n" +
                        "........\n" +
                        "........"
                ,
                4
        );

        Object move = new BestMoveFinder(gameState).findBestMove(gameState, 1).move;
        assertEquals(move, new Position(1, 0));
    }

    @Test
    void testBug2() {
        GameState gameState = new GameState(
                ".2..a...\n" +
                        ".1..#b#.\n" +
                        ".#.21...\n" +
                        "...1.#.2\n" +
                        ".#.#.211\n" +
                        "....2...\n" +
                        "..#.#1#*\n" +
                        ".....21."
                ,
                29
        );
        Object move = new BestMoveFinder(gameState).findBestMove(gameState, 3).move;
        System.out.println(move);
        assertTrue(!move.equals(Collections.singletonList(new Position(0, 7))));
    }

    @Test
    void testBug3() {
        GameState gameState = new GameState(
                "..2...1.\n" +
                ".#.#.2..\n" +
                "...a11#2\n" +
                "..#2#.b2\n" +
                "......#1\n" +
                ".......*\n" +
                ".#.#.#.1\n" +
                ".......*"
        ,
                22
        );
        Object move = new BestMoveFinder(gameState).findBestMove(gameState, 4).move;
        System.out.println(move);
        assertTrue(!move.equals(new Position(1, 7)));
    }

    @Test (enabled = false) //too slow
    void testAlphaBetaPruning() {
        GameState gameState = new GameState(
                ".*....*.\n" +
                ".#.#..#.\n" +
                "........\n" +
                "*#..#...\n" +
                "......#*\n" +
                ".#......\n" +
                "...#.#..\n" +
                ".ba....*"
                ,
                22
        );
        BestMoveFinder bmf1 = new BestMoveFinder(gameState, false);
        BestMoveFinder bmf2 = new BestMoveFinder(gameState, true);
        final int depth = 4;
        while(!gameState.finished()) {
            Object move1 = bmf1.findBestMove(gameState, depth).move;
            Object move2 = bmf2.findBestMove(gameState, depth).move;
            assertEquals(move1, move2);
            gameState = gameState.makeMove(move1);
        }
    }

    /*private void assertBetter(GameState good, GameState bad, int depth, Unit evaluatingPlayer) {
        int goodEval = new BestMoveFinder(good).evaluateRec(good, depth, evaluatingPlayer);
        int badEval = new BestMoveFinder(bad).evaluateRec(bad, depth, evaluatingPlayer);
        investigate("good", good, depth);
        investigate("bad", bad, depth);
    }/**/

    private void investigate(String title, GameState gameState, int depth) {
        System.out.println(title);
        gameState.print(false);
        BestMoveFinder bestMoveFinder = new BestMoveFinder(gameState);
        for(int curDepth = depth-1; curDepth >= 1; curDepth--) {
            Object move = bestMoveFinder.findBestMove(gameState, curDepth).move;
            if (move instanceof Position) {
                gameState = gameState.newPlaceWall((Position) move);
            } else {
                gameState = gameState.newWalk((List<Position>) move);
            }
            gameState.print(false);
            System.out.println(bestMoveFinder.evaluate(gameState));
        }
    }
}
