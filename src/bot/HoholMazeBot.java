package bot;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import static bot.Unit.*;

public class HoholMazeBot implements Runnable {
    private final Statistics statistics;
    Scanner in;
    PrintWriter out;
    public static Gson gson = new Gson();
    Random rnd = new Random();
    private final int maxDepth;

    public HoholMazeBot(Socket socket, Statistics statistics, int maxDepth) throws IOException {
        this.maxDepth = maxDepth;
        in = new Scanner(socket.getInputStream());
        out = new PrintWriter(socket.getOutputStream(), true);
        this.statistics = statistics;
    }

    @Override
    public void run() {
        JsonObject msg = new JsonObject();
        msg.addProperty("type", "game-request");
        //String username = "Hohol-bot" + rnd.nextInt();
        //String username = "Hohol_bot_" + (7-maxDepth);
        String username = "Hohol_bot_" + maxDepth;
        msg.addProperty("user", username);
        msg.addProperty("game", "maze");
        out.println(msg);
        String message = in.nextLine();
        System.out.println(message);
        JsonObject data = gson.fromJson(message, JsonObject.class);
        int playerIndex = data.get("playerIndex").getAsInt();
        Unit player = playerIndex == 0 ? player1 : player2;
        JsonArray board = data.get("board").getAsJsonArray();
        GameState gameState = new GameState(board);
        BestMoveFinder bestMoveFinder = new BestMoveFinder(gameState);
        while (true) {
            gameState.print(false);
            System.out.println(username);
            if (gameState.whatUnitMovesNow() == player) {
                bestMoveFinder.stateCnt = 0;
                Object bestMove = bestMoveFinder.findBestMove(gameState, maxDepth).move;
                System.out.println("stateCnt = " + bestMoveFinder.stateCnt);
                gameState = gameState.makeMove(bestMove);
                data = new JsonObject();
                data.addProperty("type", "move");
                data.add("move", gson.toJsonTree(bestMove));
                data.addProperty("user", username);
                System.out.println("sending:");
                System.out.println(data);
                out.println(data);
            } else {
                message = in.nextLine();
                System.out.println("received:");
                System.out.println(message);
                data = gson.fromJson(message, JsonObject.class);
                if(data.get("type").getAsString().equals("verdict")) {
                    break;
                }
                JsonElement move = data.get("move");
                if (move.isJsonPrimitive() && move.getAsString().equals("skip")) {
                    gameState = gameState.newSkipMove();
                } else if (move.isJsonArray()) {
                    List<Position> steps = new ArrayList<>();
                    for (JsonElement pos : move.getAsJsonArray()) {
                        steps.add(gson.fromJson(pos, Position.class));
                    }
                    if (gameState.validMove(steps)) {
                        gameState = gameState.newWalk(steps);
                    } else {
                        //gameState.print();
                        throw new RuntimeException("invalid move");
                    }
                } else {
                    Position to = gson.fromJson(move, Position.class);
                    if (gameState.mayPlaceWall(to)) {
                        gameState = gameState.newPlaceWall(to);
                    } else {
                        //gameState.print();
                        throw new RuntimeException("invalid move");
                    }
                }
            }
            if(gameState.finished()) {
                statistics.gameFinished(gameState);
                break;
            }
        }
    }
}
