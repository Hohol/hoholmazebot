package bot;

import java.util.ArrayList;
import java.util.List;

import static bot.GameState.N;
import static bot.Unit.gold;
import static bot.Unit.*;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static bot.Utils.*;

public class BestMoveFinder {
    public static final int INFINITY = (int) 1e9;
    private static boolean simplifyMovement = true;

    private final boolean useAlphaBetaPruning;

    public BestMoveFinder(GameState initialGameState) {
        this(initialGameState, true);
    }

    public BestMoveFinder(GameState initialGameState, boolean useAlphaBetaPruning) {
        findGoldPositions(initialGameState);
        this.useAlphaBetaPruning = useAlphaBetaPruning;
    }

    public SearchResult findBestMove(GameState gameState, int maxDepth) {
        return findBestMove(gameState, maxDepth, -INFINITY, INFINITY);
    }

    static class SearchResult {
        int evaluation;
        Object move;
    }

    public int stateCnt;

    public SearchResult findBestMove(GameState gameState, int depth, double alpha, double beta) {
        /*if(true) {
            SearchResult r = new SearchResult();
            r.move = "skip";
            return r;
        }/**/
        stateCnt++;
        List<Object> moves = getMoves(gameState);
        Unit player = gameState.whatUnitMovesNow();
        SearchResult r = new SearchResult();
        r.move = "skip";
        r.evaluation = -INFINITY;

        int moveType = gameState.whatMoveType();
        for (Object move : moves) {
            int test;
            if (move.equals("skip") && moveType == 0 && simplifyMovement) {
                test = -INFINITY;
            } else {
                test = evaluateRec(gameState.makeMove(move), depth, player, alpha, beta);
            }
            if (test > r.evaluation) {
                r.evaluation = test;
                r.move = move;
            }
            alpha = max(alpha, test);
            if (useAlphaBetaPruning && alpha >= beta) {
                return r;
            }
        }
        return r;

        /*if (gameState.whatMoveType() == 0) { //walk
            if (simplifyMovement) {
                simpleMovement(gameState, r, depth);
            } else {
                for (int i = 0; i < N; i++) {
                    for (int j = 0; j < N; j++) {
                        Position pos = new Position(i, j);
                        List<Position> steps = new ArrayList<>();
                        steps.add(pos);
                        update(gameState, r, steps, player, depth);
                        steps.add(0, null);
                        for (Position goldPosition : goldPositions) {
                            if (gameState.getCell(goldPosition) == gold) {
                                steps.set(0, goldPosition);
                                update(gameState, r, steps, player, depth);
                            }
                        }
                    }
                }
            }
        } else {
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    Position pos = new Position(i, j);
                    if (gameState.mayPlaceWall(pos)) {
                        int test = evaluateRec(gameState.newPlaceWall(pos), depth, player);
                        if (test > r.evaluation) {
                            r.evaluation = test;
                            r.move = pos;
                        }
                    }
                }
            }
        }/**/
    }

    private List<Object> getMoves(GameState gameState) {
        List<Object> r = new ArrayList<>();
        Unit player = gameState.whatUnitMovesNow();
        r.add("skip");

        if (gameState.whatMoveType() == 0) { //walk
            if (simplifyMovement) {
                r.addAll(simpleMovement(gameState));
            } else {
                for (int i = 0; i < N; i++) {
                    for (int j = 0; j < N; j++) {
                        Position pos = new Position(i, j);
                        List<Position> steps = new ArrayList<>();
                        steps.add(pos);
                        if(gameState.validMove(steps)) {
                            r.add(new ArrayList<>(steps));
                        }
                        steps.add(0, null);
                        for (Position goldPosition : goldPositions) {
                            if (gameState.getCell(goldPosition) == gold) {
                                steps.set(0, goldPosition);
                                if(gameState.validMove(steps)) {
                                    r.add(new ArrayList<>(steps));
                                }
                            }
                        }
                    }
                }
            }
        } else {
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    Position pos = new Position(i, j);
                    if (gameState.mayPlaceWall(pos)) {
                        r.add(pos);
                    }
                }
            }
        }
        return r;
    }

    private List<Object> simpleMovement(GameState gameState) {
        List<List<Position>> movements = new ArrayList<>();
        //List<GameState> gameStates = new ArrayList<>();
        List<Integer> simpleEval = new ArrayList<>();
        int maxSimpleEval = -INFINITY;

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                Position pos = new Position(i, j);
                List<Position> steps = new ArrayList<>();
                steps.add(pos);

                if (gameState.validMove(steps)) {
                    movements.add(new ArrayList<>(steps));
                }

                steps.add(0, null);
                for (Position goldPosition : goldPositions) {
                    if (gameState.getCell(goldPosition) == gold) {
                        steps.set(0, goldPosition);
                        if (gameState.validMove(steps)) {
                            movements.add(new ArrayList<>(steps));
                        }
                    }
                }
            }
        }
        for (List<Position> move : movements) {
            GameState newGameState = gameState.newWalk(move);
            int evalSimple = evaluate(newGameState);
            if (gameState.whatUnitMovesNow() != newGameState.whatUnitMovesNow()) {
                evalSimple = -evalSimple;
            }
            simpleEval.add(evalSimple);
            maxSimpleEval = max(maxSimpleEval, evalSimple);
            //gameStates.add(newGameState);
        }
        //Unit player = gameState.whatUnitMovesNow();
        List<Object> r = new ArrayList<>();
        for (int i = 0; i < movements.size(); i++) {
            int eval = simpleEval.get(i);
            if (eval != maxSimpleEval) {
                continue;
            }
            r.add(movements.get(i));
        }
        return r;
        /*List<List<Position>> movements = new ArrayList<>();
        List<GameState> gameStates = new ArrayList<>();
        List<Integer> simpleEval = new ArrayList<>();
        int maxSimpleEval = MINUS_INFINITY;

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                Position pos = new Position(i, j);
                List<Position> steps = new ArrayList<>();
                steps.add(pos);

                if (gameState.validMove(steps)) {
                    movements.add(new ArrayList<>(steps));
                }

                steps.add(0, null);
                for (Position goldPosition : goldPositions) {
                    if (gameState.getCell(goldPosition) == gold) {
                        steps.set(0, goldPosition);
                        if (gameState.validMove(steps)) {
                            movements.add(new ArrayList<>(steps));
                        }
                    }
                }
            }
        }
        for (List<Position> move : movements) {
            GameState newGameState = gameState.newMakeMove(move);
            int evalSimple = evaluate(newGameState);
            if (gameState.whatUnitMovesNow() != newGameState.whatUnitMovesNow()) {
                evalSimple = -evalSimple;
            }
            simpleEval.add(evalSimple);
            maxSimpleEval = max(maxSimpleEval, evalSimple);
            gameStates.add(newGameState);
        }
        Unit player = gameState.whatUnitMovesNow();
        for (int i = 0; i < movements.size(); i++) {
            int eval = simpleEval.get(i);
            if (eval != maxSimpleEval) {
                continue;
            }
            int realEval = evaluateRec(gameStates.get(i), depth, player);
            if (realEval > r.evaluation) {
                r.evaluation = realEval;
                r.move = movements.get(i);
            }
        }/**/
    }

    public int evaluateRec(GameState gameState, int depth, Unit evaluatingPlayer, double alpha, double beta) {
        int test;
        if (depth == 1) {
            test = evaluate(gameState);
            if (gameState.whatUnitMovesNow() != evaluatingPlayer) {
                test = -test;
            }
        } else {
            if (gameState.whatUnitMovesNow() == evaluatingPlayer) {
                test = findBestMove(gameState, depth - 1, alpha, beta).evaluation;
            } else {
                test = -findBestMove(gameState, depth - 1, -beta, -alpha).evaluation;
            }
        }

        return test;
    }

    /*private void update(GameState gameState, SearchResult r, List<Position> steps, Unit evaluatingPlayer, int depth) {
        if (gameState.validMove(steps)) {
            GameState newGameState = gameState.newMakeMove(steps);
            int test = evaluateRec(newGameState, depth, evaluatingPlayer);
            if (test > r.evaluation) {
                r.evaluation = test;
                r.move = new ArrayList<>(steps);
            }
        }
    }/**/

    /*private int pathCntFactor(GameState gameState, Unit target) {
        Position p = null;
        for (Position goldPosition : goldPositions) {
            if (gameState.getCell(goldPosition) == gold) {
                p = goldPosition;
                break;
            }
        }
        if (p == null) {
            return 0;
        }

        if (PathCntFactorFinder2.oneWallCanGreatlyIncreaseShortestPath(gameState, target, p, getGoldPositions(gameState))) {
            return 1;
        }
        return 0;
    }/**/

    private boolean stupidWall(Unit[][] board, int x, int y) {
        int stupidCnt = 0;
        Unit wall = board[x][y];
        for (int i = 0; i < 4; i++) {
            int tox = x + dx[i];
            int toy = y + dy[i];
            if (!inside(tox, toy) || board[tox][toy] == neutralWall || board[tox][toy] == wall) {
                stupidCnt++;
            }
        }
        return stupidCnt >= 3;
    }

    List<Position> goldPositions = new ArrayList<>();

    private void findGoldPositions(GameState gameState) {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (gameState.getBoard()[i][j] == gold) {
                    goldPositions.add(new Position(i, j));
                }
            }
        }
    }

    private int distToNearestGold(GameState gameState, Unit target) {
        Position pos = gameState.positionOf(target);
        return Utils.distToNearestGold(gameState, pos, target, goldPositions);
    }

    public int evaluate(GameState gameState) {
        Unit player = gameState.whatUnitMovesNow();
        Unit enemy = player == player1 ? player2 : player1;
        int playerIndex = player == player1 ? 0 : 1;
        int r = 0;
        int[] score = gameState.getScore();
        int myScore = score[playerIndex], enemyScore = score[1 - playerIndex];
        r += (myScore - enemyScore) * 1000000;

//r += (pathCntFactor(gameState, enemy) - pathCntFactor(gameState, player)) * 1000;

        if (myScore + enemyScore != goldPositions.size()) {
            int myDistToNearestGold = distToNearestGold(gameState, player);
            int enemyDistToNearestGold = distToNearestGold(gameState, enemy);
            r += enemyDistToNearestGold - myDistToNearestGold;
        }

        return r;
    }
}
