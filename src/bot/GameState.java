package bot;


import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.io.PrintStream;
import java.util.*;
import static bot.Unit.*;
import static bot.HoholMazeBot.gson;

public class GameState {

    public static final int N = 8;
    public static final int STEPS_PER_MOVE = 3;
    private static final int GOLD_CNT = 5;
    private static final int GAME_LENGTH_LIMIT = 100;

    private final Unit[][] board;
    public boolean[][] cutPoint;
    private final int[] score = new int[2];
    int moveIndex;
    private int[][][][] firstPlayerBfsCache;
    private int[][][][] secondPlayerBfsCache;

    public GameState(JsonArray ar) {
        board = new Unit[N][N];
        for (int i = 0; i < N; i++) {
            Arrays.fill(board[i], empty);
        }
        for (JsonElement jsonElement : ar) {
            Cell cell = gson.fromJson(jsonElement, Cell.class);
            board[cell.x][cell.y] = cell.value;
        }
    }

    public GameState(GameState state) {
        board = new Unit[N][N];
        for (int i = 0; i < N; i++) {
            System.arraycopy(state.board[i], 0, board[i], 0, N);
        }
        System.arraycopy(state.score, 0, score, 0, score.length);
        this.moveIndex = state.moveIndex;
    }

    public GameState(String s, int moveIndex) {
        this.moveIndex = moveIndex;
        board = new Unit[N][N];
        String[] ar = s.split("\n");
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                char ch = ar[i].charAt(j);
                for (Unit unit : Unit.values()) {
                    if(unit.getChar() == ch) {
                        board[i][j] = unit;
                        break;
                    }
                }
            }
        }
    }

    public GameState newSkipMove() {
        GameState r = new GameState(this);
        r.skipMoveInternal();
        r.firstPlayerBfsCache = firstPlayerBfsCache;
        r.secondPlayerBfsCache = secondPlayerBfsCache;
        return r;
    }

    private void skipMoveInternal() {
        moveIndex++;
    }

    public GameState newPlaceWall(Position pos) {
        GameState r = new GameState(this);
        r.placeWallInternal(pos);
        return r;
    }

    private void placeWallInternal(Position pos) {
        if (whatUnitMovesNow() == player1) {
            board[pos.x][pos.y] = wall1;
        } else {
            board[pos.x][pos.y] = wall2;
        }
        moveIndex++;
    }

    public GameState newWalk(List<Position> steps) {
        GameState r = new GameState(this);
        r.makeMoveInternal(steps);
        r.firstPlayerBfsCache = firstPlayerBfsCache;
        r.secondPlayerBfsCache = secondPlayerBfsCache;
        return r;
    }

    private void makeMoveInternal(List<Position> steps) {
        for (Position step : steps) {
            makeSimpleMove(step);
        }
        moveIndex++;
    }

    public Unit[][] getBoard() {
        return board;
    }

    public Unit whatUnitMovesNow() {
        if (whoMovesNow() == 0) {
            return player1;
        } else {
            return player2;
        }
    }

    static int whoMovesNow(int moveIndex) {
        if (moveIndex >= 1) {
            moveIndex++;
        }
        moveIndex /= 2;
        return moveIndex % 2;
    }



    private void makeSimpleMove(Position pos) {
        Unit who = whatUnitMovesNow();
        clearCell(who);
        if(board[pos.x][pos.y] == gold) {
            score[whoMovesNow()]++;
        }
        board[pos.x][pos.y] = who;
    }

    private int whoMovesNow() {
        return whoMovesNow(moveIndex);
    }


    Position positionOf(Unit unit) {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if(board[i][j] == unit) {
                    return new Position(i,j);
                }
            }
        }
        throw new RuntimeException();
    }

    private void clearCell(Unit who) {
        Position pos = positionOf(who);
        board[pos.x][pos.y] = empty;
    }

    public int whatMoveType() {
        return whatMoveType(moveIndex);
    }

    private static int whatMoveType(int moveIndex) {
        if (moveIndex >= 1) {
            moveIndex++;
        }
        return moveIndex % 2;
    }

    public boolean validMove(List<Position> moves) {
        return MoveValidator.isValidMove(this, moves, STEPS_PER_MOVE);
    }

    public boolean mayPlaceWall(Position to) {
        return MoveValidator.mayPlaceWall(this, to);
    }

    public void print(boolean transpose) {
        print(transpose, System.out);
    }

    public void print(boolean transpose, PrintStream out) {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                Unit unit = transpose ? board[j][i] : board[i][j];
                out.print(unit.getChar());
            }
            out.println();
        }
        out.println("moveIndex = " + moveIndex);
        out.println();
    }

    public int[] getScore() {
        return score;
    }

    public int getMoveIndex() {
        return moveIndex;
    }

    public Unit getCell(Position position) {
        return board[position.x][position.y];
    }

    public int[][] bfs(Unit player, Position p) {
        int[][][][] cache;
        if(player == player1) {
            if(firstPlayerBfsCache == null) {
                firstPlayerBfsCache = new int[N][N][][];
            }
            cache = firstPlayerBfsCache;
        } else {
            if(secondPlayerBfsCache == null) {
                secondPlayerBfsCache = new int[N][N][][];
            }
            cache = secondPlayerBfsCache;
        }
        if(cache[p.x][p.y] == null) {
            int[][] dist = Utils.getStrictWallsForPlayer(board, player);
            Utils.bfs(dist, p);
            cache[p.x][p.y] = dist;
        }
        return cache[p.x][p.y];
    }

    public boolean finished() {
        return score[0] + score[1] == GOLD_CNT || moveIndex == GAME_LENGTH_LIMIT;
    }

    public GameState makeMove(Object move) {
        if (move.equals("skip")) {
            return newSkipMove();
        } else if (move instanceof Position) {
            return newPlaceWall((Position) move);
        } else {
            return newWalk((List<Position>) move);
        }
    }
}
