package bot;

public class Cell {
    public int x, y;
    public Unit value;

    public Cell() {}

    public Cell(int x, int y, Unit value) {
        this.x = x;
        this.y = y;
        this.value = value;
    }
}