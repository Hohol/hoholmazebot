package bot;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

public class Statistics {
    int gameCnt;
    double score[] = new double[2];
    int moveCntSum;
    PrintStream drawOut;
    int drawCnt = 0;

    public Statistics() throws IOException {
        drawOut = new PrintStream(new FileOutputStream("draw.txt"), true);
    }

    synchronized void gameFinished(GameState gameState) {
        gameCnt++;
        moveCntSum += gameState.getMoveIndex();
        int firstScore = gameState.getScore()[0];
        int secondScore = gameState.getScore()[1];
        if(firstScore > secondScore) {
            score[0]++;
        } else if(firstScore < secondScore) {
            score[1]++;
        } else {
            System.out.println("Draw!");
            drawCnt++;
            score[0] += 0.5;
            score[1] += 0.5;
            gameState.print(false, drawOut);
        }
        if(firstScore + secondScore != 5) {
            gameState.print(false, drawOut);
        }
        System.out.println("First: " + score[0] + ", second: " + score[1]);
        System.out.println("Game ended in " + gameState.getMoveIndex() + " moves");
        double firstPlayerWinRate = score[0] / gameCnt;
        System.out.println("First player winrate = " + firstPlayerWinRate * 100 + "%");
        System.out.println("Average move cnt = " + moveCntSum / gameCnt);
        System.out.println();
    }
}
