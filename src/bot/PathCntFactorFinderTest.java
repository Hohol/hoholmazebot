package bot;

import org.testng.annotations.Test;

import static bot.GameState.N;
import static org.testng.Assert.assertEquals;

@Test
public class PathCntFactorFinderTest {
    @Test
    void test() {
        check(
                "a.######\n" +
                "....####\n" +
                "#.#.####\n" +
                "#....###\n" +
                "###.b###\n" +
                "########\n" +
                "########\n" +
                "########"
                ,

                "11######\n" +
                "1111####\n" +
                "#1#1####\n" +
                "#1111###\n" +
                "###11###\n" +
                "########\n" +
                "########\n" +
                "########"
        );

        test3();

        /**/
    }

    @Test
    void test3() {
        check(
                "a..#....\n" +
                "...#....\n" +
                "........\n" +
                "##...###\n" +
                "........\n" +
                "...#....\n" +
                "...#...b\n" +
                "...#...."
                ,
                "111#....\n" +
                "111#....\n" +
                "11111...\n" +
                "##111###\n" +
                "..111111\n" +
                "...#1111\n" +
                "...#1111\n" +
                "...#1111"
        );
    }

    @Test
    void test2() {
        check(
                "...#....\n" +
                "...#....\n" +
                "..a.b...\n" +
                "##...###\n" +
                "........\n" +
                "...#....\n" +
                "...#....\n" +
                "...#...."
                ,
                "...#....\n" +
                "...#....\n" +
                "..111...\n" +
                "##111###\n" +
                "..111...\n" +
                "...#....\n" +
                "...#....\n" +
                "...#...."
        );
    }

    @Test
    void pavelTest() {
        check(
                "#..#####\n" +
                "a..b####\n" +
                "########\n" +
                "########\n" +
                "########\n" +
                "########\n" +
                "########\n" +
                "########"
                ,
                "#11#####\n" +
                "1111####\n" +
                "########\n" +
                "########\n" +
                "########\n" +
                "########\n" +
                "########\n" +
                "########"
        );
    }

    @Test
    void pavelTest2() {
        check(
                "a.#..###\n" +
                ".....###\n" +
                "#...####\n" +
                ".....###\n" +
                "..#.b###\n" +
                "########\n" +
                "########\n" +
                "########"
                ,
                "11#..###\n" +
                "1111.###\n" +
                "#111####\n" +
                ".1111###\n" +
                "..#11###\n" +
                "########\n" +
                "########\n" +
                "########"
        );
    }

    @Test
    void pavelTest3() {
        check(
                "##...###\n" +
                "##...###\n" +
                "....b..#\n" +
                "...#...#\n" +
                "..a....#\n" +
                "##...###\n" +
                "##...###\n" +
                "########"
                ,
                "##111###\n" +
                "##111###\n" +
                "1111111#\n" +
                "111#111#\n" +
                "1111111#\n" +
                "##111###\n" +
                "##111###\n" +
                "########"
        );
    }

    @Test
    void pavelTest4() {
        check(
                "##..####\n" +
                "##...###\n" +
                ".#..b..#\n" +
                ".#.#...#\n" +
                "..a.#..#\n" +
                "##.#.###\n" +
                "##...###\n" +
                "########"
                ,
                "##11####\n" +
                "##111###\n" +
                ".#111..#\n" +
                ".#1#...#\n" +
                "..1.#..#\n" +
                "##.#.###\n" +
                "##...###\n" +
                "########"
);
    }

    private void check(String s, String exS) {
        GameState gameState = new GameState(
                s
        ,
                0
        );

        int[][] actual = PathCntFactorFinder.findCellsOnSomeSimplePath(
                gameState, Unit.player1, gameState.positionOf(Unit.player1), gameState.positionOf(Unit.player2)
        );
        int[][] expected = getExpected(exS);
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                assertEquals(actual[i][j], expected[i][j], i + " " + j);
            }
        }
    }

    private int[][] getExpected(String exS) {
        String[] arEx = exS.split("\n");
        int[][] expected = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if(arEx[i].charAt(j) == '1') {
                    expected[i][j] = 1;
                }
            }
        }
        return expected;
    }
}
