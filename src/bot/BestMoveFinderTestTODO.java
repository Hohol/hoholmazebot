package bot;

import org.testng.annotations.Test;

import java.util.Collections;

import static java.util.Arrays.asList;
import static org.testng.Assert.*;
import static org.testng.Assert.assertEquals;

@Test (enabled = false)
public class BestMoveFinderTestTODO {
    @Test
    void minimizePathCount() {
        GameState gameState = new GameState(
                "#####b..\n" +
                "#######.\n" +
                "#######.\n" +
                "#######.\n" +
                "#######.\n" +
                "........\n" +
                "a######*\n" +
                "........"
                ,
                3
        );

        Object move = new BestMoveFinder(gameState).findBestMove(gameState, 4).move;
        assertEquals(move, Collections.singletonList(new Position(5, 2)));
    }

    @Test
    void testThreeWalls() {
        GameState gameState = new GameState(
                        "#####b..\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "a111*...\n" +
                        ".######.\n" +
                        "........"
                ,
                0
        );
        Object move = new BestMoveFinder(gameState).findBestMove(gameState, 1).move;
        assertEquals(move, Collections.singletonList(new Position(7, 1)));
    }

    @Test
    public void simpleMovementCounterExample() {
        GameState gameState = new GameState(
                        "*...a1..\n" +
                        "###2###.\n" +
                        "###2###*\n" +
                        "###2222.\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "#######b\n" +
                        "########"
                ,
                0
        );
        Object move = new BestMoveFinder(gameState).findBestMove(gameState, 6).move;
        System.out.println(move);
        assertEquals(move, asList(new Position(0, 7)));
    }
}
