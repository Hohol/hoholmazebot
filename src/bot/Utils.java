package bot;

import java.util.*;

import static bot.GameState.N;
import static bot.GameState.STEPS_PER_MOVE;
import static bot.Unit.*;
import static bot.Unit.wall1;
import static java.lang.Math.abs;
import static java.lang.Math.min;

public class Utils {
    public static final int NOT_VISITED = -1;
    public static final int WALL = (int) 1e6;
    public static final int OWN_WALL = -2;
    public static final int[] dx = {0, 0, 1, -1};
    public static final int[] dy = {1, -1, 0, 0};

    private static int dist(Unit[][] board, Position a, Position b, Unit player) {
        int[][] dist = getStrictWallsForPlayer(board, player);
        bfs(dist, a);
        return dist[b.x][b.y];
    }

    public static int distToNearestGold(GameState gameState, Position a, Unit player, List<Position> goldPositions) {
        int[][] dist = gameState.bfs(player, a);
        int mi = Integer.MAX_VALUE;
        Unit[][] board = gameState.getBoard();
        for (Position p : goldPositions) {
            if (board[p.x][p.y] == gold) {
                mi = min(mi, dist[p.x][p.y]);
            }
        }
        return mi;
    }

    public static void bfsFromGold(Unit[][] board, int[][] dist, Position target) {
        Queue<Position> q = new ArrayDeque<>();
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (board[i][j] == gold) {
                    q.add(new Position(i, j));
                    dist[i][j] = 0;
                }
            }
        }
        bfs(dist, q);
    }

    public static int madDist(Unit[][] board, Position a, Position b, Unit player) {
        int[][] dist = getStrictAndOwnWallsForPlayer(board, player);
        madBfs(dist, a);
        return dist[b.x][b.y];
    }

    public static void bfs(int[][] dist, Position start) {
        Queue<Position> q = new ArrayDeque<>();
        q.add(start);
        dist[start.x][start.y] = 0;
        bfs(dist, q);
    }

    public static boolean inside(int x, int y) {
        return x >= 0 && x < N && y >= 0 && y < N;
    }

    public static int[][] getStrictWallsForPlayer(Unit[][] board, Unit player) {
        int[][] r = new int[N][N];
        for (int[] row : r) {
            Arrays.fill(row, NOT_VISITED);
        }
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                Unit value = board[i][j];
                if (value == neutralWall ||
                        player == player1 && value == wall2 ||
                        player == player2 && value == wall1) {
                    r[i][j] = WALL;
                }
            }
        }
        return r;
    }

    public static int[][] getStrictAndOwnWallsForPlayer(Unit[][] board, Unit unit) {
        int[][] dist = new int[N][N];
        for (int[] row : dist) {
            Arrays.fill(row, NOT_VISITED);
        }
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                Unit value = board[i][j];
                if (value == neutralWall ||
                        unit == player1 && value == wall2 ||
                        unit == player2 && value == wall1) {
                    dist[i][j] = WALL;
                } else if (unit == player1 && value == wall1 ||
                        unit == player2 && value == wall2) {
                    dist[i][j] = OWN_WALL;
                }
            }
        }
        return dist;
    }

    public static void madBfs(int[][] dist, Position start) {
        List<Position> q = new ArrayList<>();
        int curDist = 0;
        q.add(start);
        dist[start.x][start.y] = 0;
        int[][] d2 = new int[N][N];
        while (!q.isEmpty()) {
            curDist++;
            int[][] d = subBfs(dist, q);
            q.clear();
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    if (d[i][j] <= STEPS_PER_MOVE && dist[i][j] == NOT_VISITED) {
                        dist[i][j] = curDist;
                        d2[i][j] = d[i][j];
                        q.add(new Position(i, j));
                    }
                }
            }
        }
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (dist[i][j] == OWN_WALL) {
                    dist[i][j] = WALL;
                }
                if (dist[i][j] != WALL) {
                    dist[i][j] = dist[i][j] * STEPS_PER_MOVE * STEPS_PER_MOVE + d2[i][j];
                }
            }
        }
    }

    private static int[][] subBfs(int[][] bigDist, List<Position> starts) {
        int[][] dist = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (bigDist[i][j] == WALL) {
                    dist[i][j] = WALL;
                } else {
                    dist[i][j] = NOT_VISITED;
                }
            }
        }
        Queue<Position> q = new ArrayDeque<>();
        q.addAll(starts);
        for (Position start : starts) {
            dist[start.x][start.y] = 0;
        }
        bfs(dist, q);
        return dist;
    }

    private static void bfs(int[][] dist, Queue<Position> q) {
        while (!q.isEmpty()) {
            Position p = q.remove();
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    if (abs(i + j) != 1) {
                        continue;
                    }
                    int toX = p.x + i, toY = p.y + j;
                    if (!inside(toX, toY)) {
                        continue;
                    }
                    if (dist[toX][toY] != NOT_VISITED) {
                        continue;
                    }
                    q.add(new Position(toX, toY));
                    dist[toX][toY] = dist[p.x][p.y] + 1;
                }
            }
        }
    }

    public static void fill(int[][] a, int val) {
        for (int[] row : a) {
            Arrays.fill(row, val);
        }
    }

    public static void fill(boolean[][] a, boolean val) {
        for (boolean[] row : a) {
            Arrays.fill(row, val);
        }
    }
}
