package bot;

import java.io.IOException;
import java.net.Socket;
import java.util.Random;

public class Main {

    public static void main(String[] args) throws IOException {
        Statistics statistics = new Statistics();

        for (int depth = 1; depth <= 6; depth++) {
            new Thread(new Runner(statistics, depth)).start();
        }
        //new Thread(new Runner(statistics)).start();
    }

    static class Runner implements Runnable {
        Random rnd = new Random();

        private final Statistics statistics;
        private final int maxDepth;

        public Runner(Statistics statistics, int maxDepth) {
            this.statistics = statistics;
            this.maxDepth = maxDepth;
        }

        @Override
        public void run() {
            while(true) {
                try {
                    Socket socket = new Socket("localhost", 9293);
                    new HoholMazeBot(socket, statistics, maxDepth).run();
                    Thread.sleep(1000 + rnd.nextInt(2000));
                    //socket.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }
}
