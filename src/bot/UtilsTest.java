package bot;

import org.testng.annotations.Test;

import static bot.GameState.N;
import static bot.GameState.STEPS_PER_MOVE;
import static bot.Unit.*;
import static org.testng.Assert.*;

@Test
public class UtilsTest {
    @Test
    void testMadBfs() {
        check(
                "a.......\n" +
                ".######.\n" +
                ".######.\n" +
                "1######.\n" +
                ".######.\n" +
                "1######.\n" +
                ".######.\n" +
                "1.1.1.1."
                ,
                "01112223\n" +
                "1######3\n" +
                "1######3\n" +
                "#######4\n" +
                "2######4\n" +
                "#######4\n" +
                "3######5\n" +
                "#4#5#6#5"
                ,
                "01231231\n" +
                "1######2\n" +
                "2######3\n" +
                "#######1\n" +
                "2######2\n" +
                "#######3\n" +
                "2######1\n" +
                "#2#2#2#2"
        );

        check(
                "a11.####\n" +
                "##1#####\n" +
                "##.#####\n" +
                "########\n" +
                "########\n" +
                "########\n" +
                "########\n" +
                "########\n"
                ,
                "0##1####\n" +
                "########\n" +
                "##2#####\n" +
                "########\n" +
                "########\n" +
                "########\n" +
                "########\n" +
                "########\n"
                ,
                "0##3####\n" +
                "########\n" +
                "##3#####\n" +
                "########\n" +
                "########\n" +
                "########\n" +
                "########\n" +
                "########\n"
        );

        check(
                "a11.1###\n" +
                "##1#.###\n" +
                "##.1.###\n" +
                "###1####\n" +
                "###..1##\n" +
                "####11.#\n" +
                "#####.##\n" +
                "########\n"
                ,
                "0##1####\n" +
                "####2###\n" +
                "##2#2###\n" +
                "########\n" +
                "###34###\n" +
                "######5#\n" +
                "#####5##\n" +
                "########\n"
                ,
                "0##3####\n" +
                "####2###\n" +
                "##3#3###\n" +
                "########\n" +
                "###31###\n" +
                "######3#\n" +
                "#####3##\n" +
                "########\n"
                ); /**/
    }

    private void check(String s, String exBig, String exSmall) {
        int[][] expectedBig = getExpected(exBig);
        int[][] expectedSmall = getExpected(exSmall);
        GameState gameState = new GameState(s, 0);
        int[][] dist = Utils.getStrictAndOwnWallsForPlayer(gameState.getBoard(), player1);
        Utils.madBfs(dist, gameState.positionOf(player1));
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {

                int actualBig;
                int actualSmall;
                if(dist[i][j] == Utils.WALL) {
                    actualBig = Utils.WALL;
                    actualSmall = Utils.WALL;
                } else {
                    actualBig = dist[i][j] / (STEPS_PER_MOVE * STEPS_PER_MOVE);
                    actualSmall = dist[i][j] % (STEPS_PER_MOVE * STEPS_PER_MOVE);
                }
                assertEquals(actualBig, expectedBig[i][j], "big " + i + " " + j);
                assertEquals(actualSmall, expectedSmall[i][j], "small " + i + " " + j);
            }
        }
    }

    private int[][] getExpected(String exBig) {
        String[] arEx = exBig.split("\n");
        int[][] expected = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                char ch = arEx[i].charAt(j);
                if (ch == '#') {
                    expected[i][j] = Utils.WALL;
                } else {
                    expected[i][j] = ch - '0';
                }
            }
        }
        return expected;
    }
}
