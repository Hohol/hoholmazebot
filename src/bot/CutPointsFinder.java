package bot;

import static bot.GameState.N;
import static java.lang.Math.min;

public class CutPointsFinder {
    private static final int VISITED0 = 0;

    synchronized public static boolean[][] findCutPoints(GameState gameState, Unit target) {
        Position a = gameState.positionOf(target);
        boolean[][] cutPoint = new boolean[N][N];
        int[][] dist = Utils.getStrictWallsForPlayer(gameState.getBoard(), target);
        timer = 0;
        dfs(dist, cutPoint, a.x, a.y, -1, -1);
        return cutPoint;
    }

    private static int timer;
    private static int[][] tin = new int[N][N];
    private static int[][] fup = new int[N][N];

    private static void dfs(int[][] dist, boolean[][] cutPoint, int x, int y, int parentX, int parentY) {
        dist[x][y] = VISITED0;
        tin[x][y] = fup[x][y] = timer++;
        int children = 0;
        for (int d = 0; d < 4; d++) {
            int tox = x + Utils.dx[d];
            int toy = y + Utils.dy[d];
            if (!Utils.inside(tox, toy)) {
                continue;
            }
            if (tox == parentX && toy == parentY || dist[tox][toy] == Utils.WALL) {
                continue;
            }
            if (dist[tox][toy] != Utils.NOT_VISITED)
                fup[x][y] = min(fup[x][y], tin[tox][toy]);
            else {
                dfs(dist, cutPoint, tox, toy, x, y);
                fup[x][y] = min(fup[x][y], fup[tox][toy]);
                if (fup[tox][toy] >= tin[x][y] && parentX != -1) {
                    cutPoint[x][y] = true;
                }
                children++;
            }
        }
        if (parentX == -1 && children >= 2) {
            cutPoint[x][y] = true;
        }
    }
}
