package bot;

import java.util.List;

import static bot.GameState.*;
import static bot.Unit.*;
import static bot.Utils.*;

public class PathCntFactorFinder2 {
    public static boolean oneWallCanGreatlyIncreaseShortestPath(GameState oldGameState, Unit target, Position to,
                                                                List<Position> goldPositions) {
        GameState gameState = new GameState(oldGameState);

        boolean[][] cutPoint = CutPointsFinder.findCutPoints(gameState, target);

        Position from = gameState.positionOf(target);

        int[][] dist = Utils.getStrictWallsForPlayer(gameState.getBoard(), target);

        Utils.bfsFromGold(gameState.getBoard(), dist, to);
        int initialDist = dist[from.x][from.y];

        for (int step = 0; step < STEPS_PER_MOVE; step++) {
            for (int d = 0; d < 4; d++) {
                int tox = from.x+dx[d];
                int toy = from.y+dy[d];
                if(!inside(tox,toy)) {
                    continue;
                }
                if(dist[tox][toy] == dist[from.x][from.y]-1) {
                    from.x = tox;
                    from.y = toy;
                    break;
                }
            }
        }

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if(gameState.getBoard()[i][j] != empty) {
                    continue;
                }
                if(cutPoint[i][j]) {
                    continue;
                }
                gameState.getBoard()[i][j] = neutralWall;

                int curDist = Utils.distToNearestGold(gameState, from, target, goldPositions);

                if(curDist - initialDist >= 1) {
                    return true; // warning, board is changed
                }
                gameState.getBoard()[i][j] = empty;
                gameState = new GameState(gameState);
            }
        }
        return false;
    }
}
