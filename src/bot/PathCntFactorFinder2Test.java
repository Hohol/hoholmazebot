package bot;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.*;

@Test
public class PathCntFactorFinder2Test {
    @Test
    void test() {
        GameState gameState = new GameState(
                        "#####b..\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "........\n" +
                        "a######*\n" +
                        "........"
                ,
                0
        );
        assertTrue(PathCntFactorFinder2.oneWallCanGreatlyIncreaseShortestPath(
                gameState,
                Unit.player1,
                new Position(6, 7),
                getGoldPositions(gameState)
        ));

        test2();
    }

    @Test
    void test2() {
        GameState gameState;
        gameState = new GameState(
                "#####b..\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "#######.\n" +
                        "..a.....\n" +
                        ".######*\n" +
                        "........"
                ,
                0
        );
        assertTrue(
                PathCntFactorFinder2.oneWallCanGreatlyIncreaseShortestPath(
                        gameState,
                        Unit.player1,
                        new Position(6, 7),
                        getGoldPositions(gameState)
                )
        );
    }

    private List<Position> getGoldPositions(GameState gameState) {
        Unit[][] board = gameState.getBoard();
        List<Position> r = new ArrayList<>();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == Unit.gold) {
                    r.add(new Position(i, j));
                }
            }
        }

        return r;
    }
}
