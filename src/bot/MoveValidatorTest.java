package bot;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import static bot.Unit.*;

import static org.testng.Assert.*;
import static org.testng.Assert.assertEquals;

@Test
public class MoveValidatorTest {
    GameState gameState;

    @Test
    void testWalk() {
        gameState = new GameState(
                "a.......\n" +
                "........\n" +
                "........\n" +
                "........\n" +
                "........\n" +
                "........\n" +
                "........\n" +
                "........",
               0
        );
        check(new int[][] {
                {0, 1, 1, 1, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0, 0, 0},
                {1, 1, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
        },
                3
        );

        gameState = new GameState(
                "ab......\n" +
                "........\n" +
                "........\n" +
                "........\n" +
                "........\n" +
                "........\n" +
                "........\n" +
                "........",
                0
        );

        check(new int[][]{
                {0, 0, 1, 1, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0, 0, 0},
                {1, 1, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
        },
                3
        );

        gameState = new GameState(
                "ab......\n" +
                "#.......\n" +
                "........\n" +
                "........\n" +
                "........\n" +
                "........\n" +
                "........\n" +
                "........",
                0
        );

        check(new int[][] {
                {0, 0, 1, 1, 0, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
        },
                3
        );
    }

    @Test
    void testWall() {
        gameState = new GameState(
                ".a......\n" +
                ".......1\n" +
                "........\n" +
                ".......1\n" +
                "......2.\n" +
                "........\n" +
                "......#.\n" +
                ".2..b.#.",
                0
        );

        checkWall(new int[][]{
                {1, 0, 1, 1, 1, 1, 0, 1},
                {1, 1, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 0, 1},
                {1, 1, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 0, 1},
                {1, 1, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 0, 0},
                {1, 0, 1, 1, 0, 1, 0, 1},
        });
    }

    @Test
    void testSamePositionBug() {
        gameState = new GameState(
                "a.......\n" +
                        "........\n" +
                        "........\n" +
                        "........\n" +
                        "........\n" +
                        "........\n" +
                        "........\n" +
                        "........",
                0
        );
        assertTrue(MoveValidator.isValidMove(gameState,
                moves(new Position(0,1), new Position(0,0)),3
        ));
    }

    @Test
    void testBug() {
        gameState = new GameState(
                        "1.2...a.\n" +
                        "*#.#2#..\n" +
                        "...b....\n" +
                        "..#..#..\n" +
                        "........\n" +
                        ".....#..\n" +
                        "..#....*\n" +
                        "*...*...",
                8
        );
        assertTrue(
                gameState.mayPlaceWall(new Position(2,1))
        );
    }

    private void checkWall(int[][] expected) {
        gameState.moveIndex = 4;
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[i].length; j++) {
                boolean b = expected[i][j] == 1;
                assertEquals(MoveValidator.mayPlaceWall(gameState, new Position(i,j)), b, i + " " + j);
            }
        }
    }

    private void check(int[][] expected, int steps) {
        for (int i = 0; i < GameState.N; i++) {
            for (int j = 0; j < GameState.N; j++) {
                boolean b = expected[i][j] == 1;
                assertEquals(MoveValidator.isValidMove(gameState, moves(new Position(i,j)), steps), b);
            }
        }
    }

    private List<Position> moves(Position... moves) {
        return Arrays.asList(moves);
    }
}
